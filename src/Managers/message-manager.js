class MessageManager {
    constructor(wagner) {
      this.wagner = wagner;
      this.messageModal = this.wagner.get('message');
    }

    async createMessage(data) {
        const event = await this.messageModal.create(data);
        if (event)
          return event;
        throw new Error('No messages');
    }

    async getMessages(){
        const event = await this.messageModal.find();
        if (event)
          return event;
        throw new Error('No messages');
    }

}

module.exports = MessageManager;

