class UsersManager {
    constructor(wagner) {
      this.wagner = wagner;
      this.usersModel = this.wagner.get('users');
    }

    async createUser(data){
        const user = await this.usersModel.create(data);
        if (user)
          return user;
        throw new Error('User Not created');
    }
    
    async findUser(id){
        const user = await this.usersModel.findById(id);
        if (user)
          return user;
        throw new Error('User not found');
    }
    
    async updateUser(id, data){
      const user = await this.usersModel.updateOne({ _id: id}, data);
      if (user)
          return user;
        throw new Error('Data not updated');
    }

    async findUserbyEmail(email){
      const user = await this.usersModel.findOne({ email: email});
      if (user)
          return user;
        throw new Error('Data not updated');
    }

}

module.exports = UsersManager;

