class EventsManager {
    constructor(wagner) {
      this.wagner = wagner;
      this.eventsModal = this.wagner.get('event');
    }

    async createEvent(data) {
        const event = await this.eventsModal.create(data);
        if (event)
          return event;
        throw new Error('Event Not created');
    }

    async getAllEvents(){
        const event = await this.eventsModal.find();
        if (event)
          return event;
        throw new Error('Events Not Found');
    }

    async findParticularEvent(eventId){
        const event = await this.eventsModal.findOne({ _id:eventId });
        if (event)
          return event;
        throw new Error('Event Not Found');
    }

    async editParticularEvent(eventId, events){
        const event = await this.eventsModal.findOneAndUpdate({ _id:eventId }, events);
        if (event)
          return event;
        throw new Error('Event Not Found');
    }

    async deleteEvent(eventId){
        const event = await this.eventsModal.findOneAndDelete({ _id:eventId });
        if (event)
          return event;
        throw new Error('Event Not Found');
    }

}

module.exports = EventsManager;

