module.exports = (wagner) => {
    wagner.factory('UsersManager', () => {
        const UsersManager = require('./users-manager');
        return new UsersManager(wagner);
      });
      wagner.factory('EventsManager', () => {
        const UsersManager = require('./events-manager');
        return new UsersManager(wagner);
      });
      wagner.factory('MessageManager', () => {
        const MessageManager = require('./message-manager');
        return new MessageManager(wagner);
      });
};