module.exports = (wagner) => {
    wagner.factory('Constants', () => require('./constants'));
    wagner.factory('CipherCalls', () => require('./cipher-call'));
    wagner.factory('Mailer', () => require('./mailer'));
  };
  