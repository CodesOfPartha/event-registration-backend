module.exports = {
    MAIL_DATA: {
        SIGNUP: {
            SUBJECT: "Email confirmation for registering Events",
            BODY: "Click on this link to confirm your email : http://localhost:3000/verify/",
        }
    },
    ACCESS_TOKEN: {
        MAIL: "Mail",
        USER: "User",
    },
    STATUS_CODES: {
        //For Success Response
        SUCCESS: 200,
        //For error response | Not Found
        ERROR: 404,
        //Invalid authtoken or refresh token
        UNAUTHORIZED_ACCESS: 401,
        //No access to do particular operation
        FORBIDDEN: 403,
        //Param needs are not sufficient
        BAD_REQUEST: 400,
        //Server Error (Server cannot process this error due to some unknown circumstances)
        INTERNAL_SERVER_ERROR: 500,
        //Gateway Timeout (Request timeout limit reached)
        GATEWAY_TIMEOUT: 504,
        //Duplicate Key in MongodB Collection
        DUPLICATE_KEY_IN_COLLECTION: 409,
        //Already Authorized (only for mail token verification)
        ALREADY_AUTHORIZED: 405,
        //Email / Password is wring Basically Authentication error code
        AUTHENTICATION_ERROR: 402,
        //No data is associated with the particular User
        NO_DATA_ASSOCIATED: 406
    },
    //May be success response or failure response
    RESPONSE_MESSAGES: {
        //For Unauthorized Access
        UNAUTHORIZED_ACCESS: "Unauthorized Access",
        //Forbidden Access
        FORBIDDEN_ACCESS: "Forbidden Access",
        //If the model is not found in DB
        MODEL_NOT_FOUND: "Model Not Found",
        //MongoDb Internal Storage Error
        INTERNAL_STORAGE_ERROR: "Internal Storage Error",
        //duplicate key in collection
        DUPLICATE_KEY_IN_COLLECTION: "Duplicate Key in Collection",
        //No data is retreived forr the given filter
        NO_MATCHING_DOCUMENTS: "No Matching Documents Found",
        //No matching documents found and hence no update is performed
        NO_MATCHING_DOCUMENTS_SO_NO_UPDATE: "Update document is unsuccessful as no document is found with matching query",
        //Organization Id not Found
        ORAGANIZATION_NOT_FOUND: "Organization Id required"
    }
};