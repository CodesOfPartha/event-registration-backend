const nodemailer = require('nodemailer');

//Email Object Template {id:"",subject:"",body:""} 
module.exports.sendMail = async (email) => {
  try {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      auth: {
        user: 'Your email here',
        pass: 'password here'
      }
    });
    await transporter.sendMail(
      { to: email.id, subject: email.subject, text: email.body },
      (error, info) => {
        if (error)
          console.log(error);
        else
          console.log('Email sent: ' + info.response);
      });
  }
  catch (error) {
    error.status = error.status || 400;
    return { code: error.status, data: { Error: error.message } };
  }
};