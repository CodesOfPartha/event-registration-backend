const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
    if(!req.headers.authorization)
        return res.status(401).json({
            message : "Auth token missing"
        });
    jwt.verify(req.headers.authorization,process.env.AT_SECRET,(err,decoded)=>{
        if(decoded){
            next();
        }else{
            res.status(401).json(err.message);
        }
    });
  };
  