const jwt = require('jsonwebtoken');
const CryptoJS = require("crypto-js");

module.exports.encryption = async (data, secret_key) => {
    return CryptoJS.AES.encrypt(
      JSON.stringify(data),
      secret_key
    ).toString();
  };
  
  module.exports.decryption = async (enc_data, secret_key) => {
    var bytes = await CryptoJS.AES.decrypt(enc_data, secret_key);
    return await JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  };
  

module.exports.generateJWTToken = async (payload, secret, life) => {
    return jwt.sign(
      payload,
      secret,
      { expiresIn: life });
};

module.exports.decodeJWTToken = (token, secret) => {
    const decode = jwt.verify(token, secret);
    return  decode;
};

module.exports.apiAuthorization = () => async(req, res, next) => {
    if (!req.headers.authorization) {
        return res.sendStatus(401);
    }
    next();
};