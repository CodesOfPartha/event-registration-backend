require('dotenv').config();
const config = require('config');
const mongoose = require('mongoose');
const express = require("express");
const expressValidator = require('express-validator');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const wagner = require('wagner-core');
const swaggerDocument = require('./swagger.json');

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(helmet());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(expressValidator({
  customValidators: {
    isArray(value) {
      return Array.isArray(value);
    },
    notEmpty(array) {
      if (!array) {
        return false;
      }
      return array.length > 0;
    },
    isCoordinates(array) {
      return array.length === 2;
    },
  },
}));

// DB connection Start
mongoose.connect(config.db.url, {
  dbName: config.db.name,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
})
  .then(() => console.log('DB Connected!'))
  .catch((err) => console.log(err));
wagner.factory('mongoose', () => mongoose);
require('./Routes/index')(app, wagner);
require('./Controllers')(wagner);
require('./Managers')(wagner);
require('./Models')(wagner);
require('./Utils')(wagner);

module.exports = app;