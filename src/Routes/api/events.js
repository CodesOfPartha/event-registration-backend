const apiAuthorization = require('../../Utils/Middleware/apiAuthorization');

module.exports = (app, wagner) => {
    app.post('/api/events/create', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').createEvents(req.body, req.headers.authorization);
        res.status(result.code).json(result.data);
      });
      app.get('/api/events', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').getAllEvents(req.headers.authorization);
        res.status(result.code).json(result.data);
      });
      app.get('/api/events/:id', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').getParticularEvent(req.params.id, req.headers.authorization);
        res.status(result.code).json(result.data);
      });
      app.patch('/api/events/:id', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').patchEvents(req.body, req.params.id);
        res.status(result.code).json(result.data);
      });
      app.delete('/api/events/:id', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').deleteEvents(req.params.id, req.headers.authorization);
        res.status(result.code).json(result.data);
      });
      app.get('/api/register/events/:id', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').registerEvents(req.params.id, req.headers.authorization);
        res.status(result.code).json(result.data);
      });
      app.get('/api/cancel/events/:id', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').cancelEventRegistration(req.params.id, req.headers.authorization);
        res.status(result.code).json(result.data);
      });
      app.get('/api/user/notification', apiAuthorization, async (req, res) => {
        const result = await wagner.get('EventsController').getAllMessages(req.headers.authorization);
        res.status(result.code).json(result.data);
      });
};
