const apiAuthorization = require('../../Utils/Middleware/apiAuthorization');

module.exports = (app, wagner) => {
    app.post('/api/auth/signup', (req, res, next) => {
      req.check('email', 'Code is required').notEmpty();
      req.check('email', 'Enter a valid email id').isEmail();
      const errors = req.validationErrors();
      if (errors) 
        return res.status(400).json({ message: errors[0].msg });
      next();
      return null;
    }, async (req, res) => {
      const result = await wagner.get('UsersController').createUser(req.body);
      res.status(result.code).json(result.data);
    });
  
    app.get('/api/auth/verify/:token', async (req, res) => {
      const result = await wagner.get('UsersController').verifyUser(req.params.token);
      res.status(result.code).json(result.data);
    });

    app.post('/api/auth-token', (req, res, next) => {
      req.check('refreshToken', 'refreshToken is required').notEmpty();
      const errors = req.validationErrors();
      if (errors) 
        return res.status(400).json({ message: errors[0].msg });
      next();
      return null;
    }, async (req, res) => {
      const result = await wagner.get('UsersController').createAuthToken(req.body);
      res.status(result.code).json(result.data);
    });

    app.post('/api/user/update', apiAuthorization, (req, res, next) => {
      req.check('name', 'name is required').notEmpty();
      req.check('password', 'password is required').notEmpty();
      const errors = req.validationErrors();
      if (errors) 
        return res.status(400).json({ message: errors[0].msg });
      next();
      return null;
    }, async (req, res) => {
      const result = await wagner.get('UsersController').updateUser(req.body, req.headers.authorization);
      res.status(result.code).json(result.data);
    });

    app.post('/api/user/login', (req, res, next) => {
      req.check('email', 'email is required').notEmpty();
      req.check('password', 'password is required').notEmpty();
      const errors = req.validationErrors();
      if (errors) 
        return res.status(400).json({ message: errors[0].msg });
      next();
      return null;
    }, async (req, res) => {
      const result = await wagner.get('UsersController').userLogin(req.body);
      res.status(result.code).json(result.data);
    });
  };
  