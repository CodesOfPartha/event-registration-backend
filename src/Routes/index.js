module.exports = (app, wagner) => {
    require('./api/auth')(app, wagner);
    require('./api/events')(app, wagner);
    app.get('/health', (req, res) => res.sendStatus(200));
  };
  