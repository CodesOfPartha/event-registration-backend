module.exports = (mongoose) => {
    const eventSchema = new mongoose.Schema({
      name: {
        type: String,
        index: true,
      },
      description: {
        type: String,
        index: true,
      },
      duration: {
        type: Number,
        index: true,
      },
      location: {
        type: String,
        index: true,
      },
      fees: {
        type: Number,
        index: true,
      },
      tags: {
        type: Array,
        index: true,
      },
      fields: {
        type: Array,
        index: true,
      },
      maxParticipant: {
        type: Number,
        index: true,
      },
      participantDetails: {
        type: Array,
        index: true,
      },
      createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      }
    }, {
      timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    });
    return mongoose.model('Event', eventSchema);
  };
  