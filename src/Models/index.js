module.exports = (wagner) => {
    const mongoose = wagner.get('mongoose');
    wagner.factory('message', () => require('./message')(mongoose));
    wagner.factory('users', () => require('./user')(mongoose));
    wagner.factory('event', () => require('./event')(mongoose));
  };
  