module.exports = (mongoose) => {
    const messageSchema = new mongoose.Schema({
      title: {
        type: String,
        index: true,
      },
      message: {
        type: String,
        index: true,
      },
      userId: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      }]
    }, {
      timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    });
    return mongoose.model('Message', messageSchema);
  };
  