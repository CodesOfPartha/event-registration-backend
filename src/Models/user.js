module.exports = (mongoose) => {
    const userSchema = new mongoose.Schema({
      email: {
        type: String,
        index: true,
        unique: true,
      },
      name: {
        type: String,
        index: true,
      },
      password: {
        type: String,
        index: true,
      },
      createdEvents: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event',
      }],
      registeredEvents: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event',
      }],
      message: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message',
      }],
    }, {
      timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    });
    return mongoose.model('User', userSchema);
  };
  