// require all dependencies from app.js file
const app = require('./app');

// listen port
app.set('port', (process.env.PORT || 8080));
app.listen(app.get('port'), () => {
    console.log('Node server is running on port no:' + app.get('port'));
});