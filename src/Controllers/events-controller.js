class EventsController {
    constructor(wagner) {
        this.wagner = wagner;
        this.cipherCall = this.wagner.get('CipherCalls');
        this.eventsManager = this.wagner.get('EventsManager');
        this.usersManager = this.wagner.get('UsersManager');
        this.messageManager = this.wagner.get('MessageManager');
    }

    async createEvents(event, token){
        try{
            const decode = this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            event.createdBy = decode.userId;
            const result = await this.eventsManager.createEvent(event);
            const userDetails = await this.usersManager.findUser(decode.userId);
            userDetails.createdEvents.unshift(result._id);
            const updateUser = await this.usersManager.updateUser(decode.userId, userDetails);
            if (result) 
                return { code: 201, data:  {message: 'Event created successfully' }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async getAllEvents(token){
        try{
            const decode = this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            const userDetails = await this.usersManager.findUser(decode.userId);
            const result = await this.eventsManager.getAllEvents();
            let createdEvents = [];
            let registeredEvents = [];
            let overallEvents = [];
            result.map((event)=>{
                const temp = {
                    _id:event.id,
                    name:event.name,
                    description: event.description,
                    duration: event.duration,
                    fees: event.fees,
                    maxParticipant: event.maxParticipant,
                    fields: event.fields,
                    registeredParticipants: event.participantDetails.length,
                    tags: event.tags
                };
                if (userDetails.createdEvents.includes(event._id))
                    createdEvents.push(event);
                else if (userDetails.registeredEvents.includes(event._id))
                    registeredEvents.push(temp);
                else 
                    overallEvents.push(temp);
            });

            if (result) 
                return { code: 201, data:  { events: {
                    createdEvents: createdEvents,
                    registeredEvents: registeredEvents,
                    overallEvents: overallEvents
                }, totalCount: result.length }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async getParticularEvent(eventId, token){
        try {
            const decode = this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            const userDetails = await this.usersManager.findUser(decode.userId);
            const eventDetails = await this.eventsManager.findParticularEvent(eventId);
            let temp = undefined;
            if(userDetails.createdEvents.includes(eventDetails._id)){
                temp = eventDetails;
            }    
            else{
                temp = {
                    _id:eventDetails._id,
                    name:eventDetails.name,
                    description: eventDetails.description,
                    duration: eventDetails.duration,
                    fees: eventDetails.fees,
                    maxParticipant: eventDetails.maxParticipant,
                    fields: eventDetails.fields,
                    registeredParticipants: eventDetails.participantDetails,
                    tags: eventDetails.tags
                };
            }
            return { code: 200, data:  {data: temp }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async patchEvents(events, eventId){
        try {
            const eventDetails = await this.eventsManager.editParticularEvent(eventId, events);
            return { code: 200, data:  {message: "Events updated successfully" }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async deleteEvents(eventId, token){
        try {
            const eventDetails = await this.eventsManager.findParticularEvent(eventId);
            const decode = await this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            let temp = undefined;
            if(decode.userId == eventDetails.createdBy){
                const deletedEvent = await this.eventsManager.deleteEvent(eventId);
                if(eventDetails.participantDetails.length>0){
                    temp={
                        title: eventDetails.name+' has been deleted',
                        message: eventDetails.description,
                        userId: eventDetails.participantDetails,
                    };
                }
            } else{
                return { code: 400, data:  {message: "User don't have access to delete it!" }  };
            }
            if(temp){
                const MessageUpdate = await this.messageManager.createMessage(temp);
            }
            return { code: 200, data:  {message: "deleted successfully" }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async registerEvents(eventId, token){
        try {
            const eventDetails = await this.eventsManager.findParticularEvent(eventId);
            const decode = await this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            eventDetails.participantDetails.unshift(decode.userId);
            const updatedEvent = await this.eventsManager.editParticularEvent(eventId, eventDetails);
            const userDetails = await this.usersManager.findUser(decode.userId);
            userDetails.registeredEvents.unshift(eventDetails._id);
            const result = await this.usersManager.updateUser(decode.userId, userDetails);
            return { code: 200, data:  {message: "Registered successfully" }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async cancelEventRegistration(eventId, token){
        try {
            const eventDetails = await this.eventsManager.findParticularEvent(eventId);
            const decode = await this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            const index = eventDetails.participantDetails.indexOf(decode.userId);
            eventDetails.participantDetails.splice(index,1);
            const userDetails = await this.usersManager.findUser(decode.userId);
            const userIndex=userDetails.registeredEvents.indexOf(eventId);
            userDetails.registeredEvents.splice(userIndex, 1);
            const result = await this.usersManager.updateUser(decode.userId, userDetails);
            const updatedEvent = await this.eventsManager.editParticularEvent(eventId, eventDetails);
            return { code: 200, data:  {message: "Cancellation successfull" }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async getAllMessages(token){
        try {
            const decode = await this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            const messages = await this.messageManager.getMessages(decode.userId);
            let temp = [];
            await messages.map((message)=>message.userId.includes(decode.userId)?temp.unshift({title:message.title, message:message.message}):null);
            return { code: 200, data:  {messages: temp }  };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

}

module.exports = EventsController;

