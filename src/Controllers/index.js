module.exports = (wagner) => {
    wagner.factory('UsersController', () => {
        const UsersController = require('./users-controller');
        return new UsersController(wagner);
      });
    
      wagner.factory('EventsController', () => {
        const EventsController = require('./events-controller');
        return new EventsController(wagner);
      });
};