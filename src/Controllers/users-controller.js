class UserController {
    constructor(wagner) {
      this.wagner = wagner;
      this.usersManager = this.wagner.get('UsersManager');
      this.cipherCall = this.wagner.get('CipherCalls');
      this.mailer  = this.wagner.get('Mailer');
      this.messageManager = this.wagner.get('MessageManager');
      this.constants = this.wagner.get('Constants');
    }

    async createUser(user){
        try{
            const result = await this.usersManager.createUser(user);
            const accessToken = await this.cipherCall.generateJWTToken({ userId: result._id, type: 'mail' }, process.env.SECRET, process.env.AT_LIFE);
            const mailData = {
                id: user.email,
                subject: this.constants.MAIL_DATA.SIGNUP.SUBJECT,
                body: this.constants.MAIL_DATA.SIGNUP.BODY + accessToken
            };
            this.mailer.sendMail(mailData);
            if (result) 
                return { code: 201, data: { message: 'User Created successfully! Confirm verification using email' } };
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async verifyUser(token){
        try{
            const decode = await this.cipherCall.decodeJWTToken(token, process.env.SECRET);
            const verifyUser = await this.usersManager.findUser(decode.userId);
            if (verifyUser) {
                const refreshToken = await this.cipherCall.generateJWTToken({ userId: decode.userId, refresh: true }, process.env.RT_SECRET, process.env.RT_LIFE);
                return { code: 200, data: { message: 'User verified successfully!', refreshToken:  refreshToken} };
            }
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }
    async createAuthToken(token){
        try{
            const decode = await this.cipherCall.decodeJWTToken(token.refreshToken, process.env.RT_SECRET);
            const verifyUser = await this.usersManager.findUser(decode.userId);
            if (verifyUser) {
                const authToken = await this.cipherCall.generateJWTToken({ userId: decode.userId, auth: true }, process.env.AT_SECRET, process.env.AT_LIFE);
                return { code: 200, data: { authToken:  authToken} };
            }
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }
    async updateUser(data, token){
        try{
            const decode = this.cipherCall.decodeJWTToken(token, process.env.AT_SECRET);
            let temp = undefined;
            if (data.password)
                data.password = await this.cipherCall.encryption(data.password, process.env.SECRET);
           if(decode){
                const result = await this.usersManager.updateUser(decode.userId, data);
                temp={
                    title: 'Hey '+data.name+'! Welcome to Event Registration App',
                    message: 'We welcome you to explore the next generation of events.',
                    userId: [decode.userId],
                };
                const MessageUpdate = await this.messageManager.createMessage(temp);
                data.message=[MessageUpdate._id];
                const updateMessage = await this.usersManager.updateUser(decode.userId, data);
                return { code: 200, data: result };
           }
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }

    async userLogin(data){
        try{
            const verifyUser = await this.usersManager.findUserbyEmail(data.email);
            const password = await this.cipherCall.decryption(verifyUser.password, process.env.SECRET);
           if(data.email === verifyUser.email && data.password === password){
                const refreshToken = await this.cipherCall.generateJWTToken({ userId: verifyUser._id, refresh: true }, process.env.RT_SECRET, process.env.RT_LIFE);
                return { code: 200, data: { refreshToken: refreshToken } };
           }else{
            return { code: 400, data: {message:"password not matching"} };
           }
        } catch(error){
            error.status = error.status || 400;
            return { code: error.status, data: { Error: error.message } };
        }
    }
}

module.exports = UserController;

